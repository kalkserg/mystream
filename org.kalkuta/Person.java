import java.awt.event.PaintEvent;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Person {
    private String name;
    private List<Animal> animals = new ArrayList<>();

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    interface Animal {
        String getName();
        int getAge();
        String voice();
    }

    static class Cat implements Animal {

        private String name;
        private int age;

        public Cat(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getAge() {
            return age;
        }

        @Override
        public String voice() {
            return "Мяууу";
        }

        @Override
        public String toString() {
            return "Cat{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cat cat = (Cat) o;
            return age == cat.age &&
                    Objects.equals(name, cat.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, age);
        }
    }

    static class Dog implements Animal {
        private String name;
        private int age;

        public Dog(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getAge() {
            return age;
        }

        @Override
        public String voice() {
            return "Гав!";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Dog dog = (Dog) o;
            return age == dog.age &&
                    Objects.equals(name, dog.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, age);
        }

        @Override
        public String toString() {
            return "Dog{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    public static void main(String[] args) {
        Person vito = new Person("Vito");
        vito.getAnimals().add(new Dog("Sharik", 3));
        vito.getAnimals().add(new Cat("Tom", 5));

        Person michael = new Person("Michael");
        michael.getAnimals().add(new Cat("Pushok", 1));
        michael.getAnimals().add(new Dog("Barsik", 7));
        michael.getAnimals().add(new Dog("Sharik", 5));

        Person sarah = new Person("Sarah");
        sarah.getAnimals().add(new Cat("Kiki", 3));
        sarah.getAnimals().add(new Cat("Tom", 2));
        sarah.getAnimals().add(new Dog("Piki", 10));
        sarah.getAnimals().add(new Dog("Tiki", 8));

        //List<Person> persons = Arrays.asList(vito, michael, sarah);// List.of(vito, michael, sarah);
        List<Person> persons = List.of(vito, michael, sarah);// List.of(vito, michael, sarah);
//        List<Animal> animals = new ArrayList<>();
//        animals.addAll(vito.getAnimals());
//        animals.addAll(michael.getAnimals());
//        animals.addAll(sarah.getAnimals());

//        1) Печатает список всех животных которые есть у всех людей
        persons.stream()
                .map(Person::getAnimals)
                .flatMap(List<Animal>::stream)
                .map(Animal::getName)
                .forEach(System.out::println);

//        2) Возвращает коллекцию животных, которые есть у всех людей
        List<Animal> animals = persons.stream()
                .map(Person::getAnimals)
                .flatMap(List<Animal>::stream)
                .collect(Collectors.toList());
        System.out.println(animals);

//        3) Возвращает список всех собак
        List<Animal> dogs = persons.stream()
                .map(Person::getAnimals)
                .flatMap(List<Animal>::stream)
                .filter(it -> it.getClass() == Dog.class)
                .collect(Collectors.toList());
        System.out.println(dogs);

//        4) Возвращает среднее арифметическое возраста всех котов с именем НЕ Том
        Double ageCat = persons.stream()
                .map(Person::getAnimals)
                .flatMap(List<Animal>::stream)
                .filter(it -> it.getClass() == Cat.class)
                .filter(it -> it.getName() != "Tom")
                .mapToInt(Animal::getAge)
                .average()
                .orElse(0.0D);
        System.out.println(ageCat);

//        5) Вызывает метод voice у всех животных, которые старше 4 лет
        persons.stream()
                .map(Person::getAnimals)
                .flatMap(List<Animal>::stream)
                .filter(it -> it.getAge() > 4)
                .forEach(it -> System.out.println(it.voice()));

//        6) Возвращает коллекцию людей у которых больше  2х животных
        persons.stream()
                .filter(it -> it.getAnimals().size() > 2)
                .map(it ->it.getName())
                .forEach(System.out::println);

//        7) Возвращает коллекцию всех имен котов (без дубликатов), отсортированную
        List<String> catsName = persons.stream()
                .map(Person::getAnimals)
                .flatMap(List<Animal>::stream)
                .filter(it -> it.getClass() == Cat.class)
                .map(it -> it.getName())
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        System.out.println(catsName);

//        8) Возвращает коллекцию всех имен собак (без дубликатов), написанных большими буквами
        List<String> dogsName = persons.stream()
                .map(Person::getAnimals)
                .flatMap(List<Animal>::stream)
                .filter(it -> it.getClass() == Dog.class)
                .map(it -> it.getName().toUpperCase())
                .distinct()
                .collect(Collectors.toList());
        System.out.println(dogsName);

//        9) Возвращает мапу в которой ключ - имя владельца, значение - массив имен его питомцев
        Map<String,String[]> mapa = persons.stream()
                .collect(Collectors.toMap(e -> e.getName(), e -> e.getAnimals()
                        .stream()
                        .map(it -> it.getName())
                        .toArray(String[]::new)));
        System.out.println(mapa);

        for(Map.Entry<String, String[]> item : mapa.entrySet()){
            System.out.printf("Key: %s  Value: \n", item.getKey());
                Arrays.stream(item.getValue())
                    .map(it -> it.toString())
                    .forEach(System.out::println);
        }

//        10) Находит любого питомца с возрастом больше 5 лет//
        Optional<Animal> anyOld = persons.stream()
                .map(Person::getAnimals)
                .flatMap(List<Animal>::stream)
                .filter(it -> it.getAge() > 5)
                .findFirst();
        System.out.println(anyOld);
    }
}
